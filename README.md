# Modified tensorflow docker

## Requires
- cuda
- docker
- nvidia-docker

<br>

## How to build
```shell
$ docker build -t dev_note .
```

<br>

## Create container
```shell
$ nvidia-docker run -it -d \
    -p 8888:8888 \
    -p 6006:6006 \
    -v /mount/path:/content \
    --restart always \
    --name dev \
    dev_note:latest
```

<br>

## jupyter notebook initial settings
- notebook dir : /content
- passwd : tfdocker

<br>

---