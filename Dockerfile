ARG TF_SERVING_VERSION=latest
ARG TF_SERVING_BUILD_IMAGE=tensorflow/serving:${TF_SERVING_VERSION}-devel-gpu

FROM ${TF_SERVING_BUILD_IMAGE} as build_image
FROM nvidia/cuda:10.0-base-ubuntu16.04

LABEL maintainer="klumblr <klumblr@gmail.com>"

ENV CUDNN_VERSION=7.4.1.5
ENV TF_TENSORRT_VERSION=5.0.2

RUN apt-get update && apt-get install -y --no-install-recommends \
        ca-certificates \
        cuda-command-line-tools-10-0 \
        cuda-command-line-tools-10-0 \
        cuda-cublas-10-0 \
        cuda-cufft-10-0 \
        cuda-curand-10-0 \
        cuda-cusolver-10-0 \
        cuda-cusparse-10-0 \
        libcudnn7=${CUDNN_VERSION}-1+cuda10.0 \
        libgomp1 \
        && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

# The 'apt-get install' of nvinfer-runtime-trt-repo-* library adds a new list
# which contains libnvinfer library, so it needs another 'apt-get update' to
# retrieve that list before it can actually install the library.
#
# We don't install libnvinfer-dev since we don't need to build against TensorRT.
RUN apt-get update && \
    apt-get install --no-install-recommends \
        nvinfer-runtime-trt-repo-ubuntu1604-${TF_TENSORRT_VERSION}-ga-cuda10.0 && \
    apt-get update && \
    apt-get install --no-install-recommends \
        libnvinfer5=${TF_TENSORRT_VERSION}-1+cuda10.0 && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* && \
    rm /usr/lib/x86_64-linux-gnu/libnvcaffe_parser* && \
    rm /usr/lib/x86_64-linux-gnu/libnvparsers*

# Install TF Serving GPU pkg
COPY --from=build_image /usr/local/bin/tensorflow_model_server /usr/bin/tensorflow_model_server

# Install libraries
## Set-up for R
RUN echo "deb https://cloud.r-project.org/bin/linux/ubuntu xenial/" >> /etc/apt/sources.list && \
    apt-key adv --keyserver keyserver.ubuntu.com --recv-keys E084DAB9 && \
    apt-get update

RUN apt-get install -y --no-install-recommends \
        software-properties-common \
        automake \
        build-essential \
        gcc \
        pkg-config \
        openssh-server \
        rsync \
        curl \
        wget \
        nano \
        vim \
        unzip \
        zip \
        git \
        tmux \
        screen \
        htop \
        locales \
        tzdata \
        libfreetype6-dev \
        libpng12-dev \
        libtool \
        libzmq3-dev \
        libsm6 \
        libxext6 \
        libopencv-dev \
        libav-tools  \
        libjpeg-dev \
        libpng-dev \
        libtiff-dev \
        libjasper-dev \
        libapparmor1 \
        libssl-dev \
        libxml2-dev \
        libcurl3-dev \
        libbz2-dev \
        libpcre3-dev \
        fonts-dejavu \
        fonts-liberation \
        fonts-nanum-coding \
        r-base \
        r-cran-rcpparmadillo \
        gfortran \
        mlocate \
        swig \
        zlib1g-dev \
        openjdk-8-jdk \
        openjdk-8-jre-headless \
        && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* 

# Set up locale
RUN echo "en_US.UTF-8 UTF-8" > /etc/locale.gen && \
    locale-gen

# Set Time zone
ENV TZ=Asia/Seoul
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

# Configure environment
ENV CONDA_DIR=/opt/conda \
    SHELL=/bin/bash \
    LC_ALL=en_US.UTF-8 \
    LANG=en_US.UTF-8 \
    LANGUAGE=en_US.UTF-8 \
    ENVPY2=py2

ENV PATH=$CONDA_DIR/bin:$PATH

# Install conda
ENV CONDA_VERSION 4.6.14
ENV CONDA_MD5 718259965f234088d785cad1fbd7de03
RUN cd /tmp && \
    wget --quiet https://repo.continuum.io/miniconda/Miniconda3-$CONDA_VERSION-Linux-x86_64.sh && \
    echo "${CONDA_MD5} Miniconda3-$CONDA_VERSION-Linux-x86_64.sh" > miniconda.md5 && \
    /bin/bash Miniconda3-${CONDA_VERSION}-Linux-x86_64.sh -f -b -p $CONDA_DIR && \
    rm Miniconda3-${CONDA_VERSION}-Linux-x86_64.sh && \
    $CONDA_DIR/bin/conda config --system --prepend channels conda-forge && \
    $CONDA_DIR/bin/conda config --system --set auto_update_conda false && \
    $CONDA_DIR/bin/conda config --system --set show_channel_urls true && \
    $CONDA_DIR/bin/conda install --quiet --yes conda="${CONDA_VERSION%.*}.*" && \
    $CONDA_DIR/bin/conda update --all --quiet --yes && \
    conda clean --all

# Install Tini
RUN conda install --quiet --yes 'tini=0.18.0' && \
    conda list tini | grep tini | tr -s ' ' | cut -d ' ' -f 1,2 \
    >> $CONDA_DIR/conda-meta/pinned && \
    conda clean --all

# Install modules
RUN conda install --quiet --yes \
        pip \
        Pillow \
        h5py \
        notebook \
        matplotlib \
        numpy \
        pandas \
        scipy \
        scikit-learn \
        scikit-image \
        opencv \
        tqdm \
        requests \
        grpcio \
        mock \
        uwsgi && \
        conda clean --all

RUN pip --no-cache-dir install \
    torch \
    torchvision \
    openpyxl \
    xlrd \
    flask-cors \
    tensorflow-gpu \
    keras \
    jupyter-tensorboard

# Venv python2
RUN conda create --quiet --yes -n ${ENVPY2} python=2.7 anaconda && \
    conda install --quiet --yes -n ${ENVPY2} \
        pip \
        Pillow \
        h5py \
        ipykernel \
        matplotlib \
        numpy \
        pandas \
        scipy \
        scikit-learn \
        scikit-image \
        opencv \
        tqdm \
        requests \
        grpcio \
        mock \
        uwsgi && \
        ${CONDA_DIR}/envs/${ENVPY2}/bin/python -m ipykernel install --user && \
        conda clean --all

RUN ${CONDA_DIR}/envs/${ENVPY2}/bin/pip --no-cache-dir install \
    tensorflow-gpu \
    keras \
    tensorflow-serving-api \
    openpyxl \
    xlrd \
    flask-cors

# Fix autocomplete
RUN ipython profile create && \
    echo "c.Completer.use_jedi = False" >> /root/.ipython/profile_default/ipython_config.py

# Get R kernel
RUN R CMD javareconf && \
    conda install -c conda-forge readline && \
    conda install -c r r-essentials r-irkernel

# Install R packages
RUN Rscript -e 'install.packages(c("data.table", "ggplot2", "gridExtra", "plyr", "dplyr", "tidyverse", \
                                 "stringr", "pROC", "tidyr", "randomForest", "gtable", "rmarkdown", "knitr", "prettydoc", \
                                 "DBI", "rJava", "xlsx", "sqldf", "RCurl", "jsonlite"), repos="http://cran.rstudio.com")' && \
    Rscript -e 'install.packages("h2o", type="source", repos="http://h2o-release.s3.amazonaws.com/h2o/rel-yates/4/R")'

# Set up our notebook config.
COPY jupyter_notebook_config.py /root/.jupyter/

# Jupyter has issues with being run directly:
#   https://github.com/ipython/ipython/issues/7062
# We just add a little wrapper script.
COPY run_jupyter.sh /

# Set work directory
WORKDIR /content

# WAS
EXPOSE 8080

# Jupyter notebook
EXPOSE 8888

# Set Volume
VOLUME ["/content"]

# Set work directory
WORKDIR "/content"

# Run Jupyter notebook
CMD ["/bin/bash", "/run_jupyter.sh", "--allow-root"]
